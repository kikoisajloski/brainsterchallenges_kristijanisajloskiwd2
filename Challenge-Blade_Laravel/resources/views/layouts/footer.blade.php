<div class="d-flex justify-content-center">
        <div class="icons d-flex justify-content-between w-25 my-5">
            <a href=""><i class="fab fa-facebook-square"></i></a>
            <a href=""><i class="fab fa-twitter-square"></i></a>
            <a href=""><i class="fab fa-github"></i></a>
        </div>
</div>
<div class="copyright text-secondary d-flex justify-content-center my-2">
    <p>Copyright &copy; Your Website 2018</p>
</div>