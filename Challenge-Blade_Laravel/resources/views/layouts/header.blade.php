<div class="header">
  <nav class="navbar navbar-expand-lg navbar-dark d-flex justify-content-between">
    <div class="container d-flex justify-content-between">
      <a class="navbar-brand" href="#">Blog</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          @yield('navbar')
        </div>
      </div>
    </div>
  </nav>
  <div class="container my-5 text-white">
    <div class="d-flex justify-content-center align-middle">
      <div class="content my-5">
        @yield('title')
      </div>
    </div>
  </div>
</div>