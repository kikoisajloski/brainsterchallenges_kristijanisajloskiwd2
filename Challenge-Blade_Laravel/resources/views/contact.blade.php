@extends('layouts.master')
@section('stylePage')
<link rel="stylesheet" href="{{asset('stylecontact.css')}}">
@endsection
@section('title')
    <h1 class="title-text">Contact Me</h1><br/>
    <p>Have questions? I have answers!</p>
@endsection
@section('navbar')
<a class="nav-link" aria-current="page" href="{{route('home')}}">Home</a>
<a class="nav-link" href="{{route('about')}}">About</a>
<a class="nav-link" href="{{route('blog')}}">Sample Post</a>
<a class="nav-link active" href="{{route('contact')}}">Contact</a>   
@endsection
@section('main-content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3 my-5">
                <input type="text" placeholder="Name" class="form-control"><br>
                <input type="text" placeholder="Email Address" class="form-control"><br>
                <input type="text" placeholder="Phone Number" class="form-control"><br>
                <textarea name="" id="" cols="30" rows="3" placeholder="Message" class="form-control"></textarea>
                <button class="btn btn-info text-white my-2">Send</button>
            </div>
        </div>
    </div>
@endsection