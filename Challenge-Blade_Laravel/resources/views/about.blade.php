@extends('layouts.master')
@section('stylePage')
<link rel="stylesheet" href="{{asset('styleabout.css')}}">
@endsection
@section('title')
    <h1 class="title-text">About Me</h1><br/>
    <p>This is what I do.</p>
@endsection
@section('navbar')
<a class="nav-link" aria-current="page" href="{{route('home')}}">Home</a>
<a class="nav-link active" href="{{route('about')}}">About</a>
<a class="nav-link" href="{{route('blog')}}">Sample Post</a>
<a class="nav-link" href="{{route('contact')}}">Contact</a> 
@endsection
@section('main-content')
    <div class="container">
        <div class="row my-3">
            <div class="col-md-6 offset-3">
                <p>Sed temporibus assumenda aut tempore debitis et ipsam distinctio a dolor voluptatem qui neque quia. Sit architecto nobis qui suscipit aliquam aut officia Quis ut molestiae deserunt sed exercitationem quia et odio aperiam sit autem deserunt. Aut voluptas internos et tenetur similique rem praesentium eligendi in totam mollitia 
                    non cumque placeat est delectus autem et quisquam ipsum. Non iusto Quis hic veritatis debitis et galisum nihil et neque natus qui harum tempora.</p>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-6 offset-3">
                <p>Sed temporibus assumenda aut tempore debitis et ipsam distinctio a dolor voluptatem qui neque quia. Sit architecto nobis qui suscipit aliquam aut officia Quis ut molestiae deserunt sed exercitationem quia et odio aperiam sit autem deserunt. Aut voluptas internos et tenetur similique rem praesentium eligendi in totam mollitia 
                    non cumque placeat est delectus autem et quisquam ipsum. Non iusto Quis hic veritatis debitis et galisum nihil et neque natus qui harum tempora.</p>
            </div>
        </div>
    </div>
@endsection