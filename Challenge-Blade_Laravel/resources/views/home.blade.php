@extends('layouts.master')
@section('stylePage')
<link rel="stylesheet" href="{{asset('stylehome.css')}}">
@endsection
@section('title')
    <h1 class="title-text">Clean Blog</h1><br/>
    <p>A Blog Theme By Start Bootstrap</p>
@endsection
@section('navbar')
<a class="nav-link active" aria-current="page" href="{{route('home')}}">Home</a>
<a class="nav-link" href="{{route('about')}}">About</a>
<a class="nav-link" href="{{route('blog')}}">Sample Post</a>
<a class="nav-link" href="{{route('contact')}}">Contact</a>  
@endsection


@section('main-content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3 my-2  border-bottom">
                <h4>Lorem Ipsum</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum harum ratione
                nulla unde iusto dolor eum nisi minus aliquid voluptatem nihil aut aperiam, debitis consequuntur voluptas. A ipsum sint similique.</p>
                <small class="text-secondary"><em>Posted by: <b>John Doe</b></em></small>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-3 my-2 border-bottom">
                <h4>Lorem Ipsum</h4>
                <small class="text-secondary"><em>Posted by: <b>John Doe</b></em></small>
            </div>
        </div>
    <div class="row">
        <div class="col-md-6 offset-3 my-2  border-bottom">
            <h4>Lorem Ipsum</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum harum ratione
            nulla unde iusto dolor eum nisi minus aliquid voluptatem nihil aut aperiam, debitis consequuntur voluptas. A ipsum sint similique.</p>
            <small class="text-secondary"><em>Posted by: <b>Jane Doe</b></em></small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-3 my-2 border-bottom">
            <h4>Lorem Ipsum</h4>
            <small class="text-secondary"><em>Posted by: <b>Messy Doe</b></em></small>
        </div>
    </div>
</div>
@endsection